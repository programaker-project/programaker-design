---
title: Collaborative Scratch program edition
---
# Programaker proposal 2: Collaborative Scratch program edition

## Goal

Provide a way for PrograMaker's programs to be edited collaboratively (like EtherPad docs).

 - This will be done in a way that keeps the **backend agnostic from changes in
   the frontend**. This means changes in the language, interface or structures
   used on the frontend won't require changes on the backend.

 - Considering the previous point, this can be done in an incremental way.

We'll consider this a naive first approach to test the usefulness of the
feature. No strong efforts on the stability of this *API* will be made, as it
won't affect the backend protocol and it's logic is local to the **frontend**.

## Changes on backend

 - The backend will add a new websocket channel, specific for each program, which the different editors might use to communicate.
     - It's URL will be
       `/api/v0/users/id/:user_id/programs/id/:program_id/editor-events`, and it
       will replicate the inputs to all the other listeners on the same channel
       (but not back to the sender).

 - Some logic might be added on the future to handle permissions:
     - Allow sending messages to the users which have `write` permissions on the program.
     - Allow receiving to all the users with `read` permissions.

## Changes on frontend

 1. Connect to the provided `editor-events` channel.
 2. Receive status updates made by other users and apply them in the local editor.
 3. Send status updates on each change made by the user.

### How to allow joining editor mid-update?

This is a naive, preliminary process to coordinate uploads with the current limitations, and with a backend that does not *understand* the underlying program structure.

#### Frontend

 0. Consider a `SAVE_TIME` of 60 seconds
 1. When a frontend joins, it receives the past events applied to the programs
 2. It waits for `SAVE_TIME * 2`, if it doesn't receive a message with `subtype = save`, it starts a save
     - If no message was sent, this frontend is now the "leader". It will repeat the process but from now on waiting for `SAVE_TIME` or for a specific number of events.
     - If a message was sent, repeat the process, again waiting for `SAVE_TIME * 2`.

 - Ideally, in case of websocket disconnection the editor must be put on read-only mode, and the program reloaded when the connection is re-established.
 - A simple alternative to this flow, which might be confusing and require significant coordination to complete successfully, is to notify the user and ask them to reload the page.
  - As a possible problem for the most complex flow consider what would happen
    if the disconnected editor has been sending messages with progress through
    the disconnected socket, this messages would have been lost, but that
    condition might not be clear to the user.


#### Backend

 - The backend will have to save all events between two editor saves and stream them back when re-connecting.

## Related MRs

 - https://gitlab.com/programaker-project/programaker-core/-/merge_requests/168
